#include "pch.h"
#include "game.h"


Game::Game(const Player& Player)
	:_NumberGenerator(1, 10)
{
	Player_ = Player;
}

/*Game::Game(const Game &)
	:_NumberGenerator(1, 10)
{
}*/

Game::~Game(){}

void Game::star_game()
{
	std::cout << "Enter your name : ";
	std::getline(std::cin, Player_.name);
	std::cout << "Enter start money amount :";
	std::cin >> Player_.total_money;
}

bool Game::keep_playing()
{	
	char responce;
	std::cout << "Play again? (Y/N)";
	std::cin >> responce;
	return responce == 'Y' || responce == 'y';
}

int Game::get_bet_amount()
{
	std::cout << "Enter bet amount (1 - " 
		+ std::to_string(Player_.total_money) + ")";
	int Wager;
	std::cin >> Wager;
	return Wager;
}

int Game::get_guess()
{
	std::cin.get();
	std::cout << "Enter guess (1-10): ";
	int Guess;
	std::cin >> Guess;

	return Guess;
}

WagerResult Game::play_around()
{
	const auto Wager = get_bet_amount();
	const auto Guess = get_guess();

	WagerResult result;
	result.wager = Wager;
	result.guess = Guess;

	result.random_number = _NumberGenerator.generate_random_numbers();

	if (result.random_number == Guess)
	{
		result.did_win = true;
		result.money_change_amount = Wager * 10;
	}
	else
	{
		result.did_win = false;
		result.money_change_amount = -1 * Wager;
	}
	
	Player_.total_money += result.money_change_amount;
	return result;
}

bool Game::game_over()
{
	return Player_.total_money <= 0;
}