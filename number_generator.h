#pragma once
#include <random>

class NumberGenerator
{
public:
	NumberGenerator(const int &min, const int &max);
	virtual ~NumberGenerator();
	int generate_random_numbers();
private:
	std::random_device _random_device;
	std::default_random_engine _default_random_engine;
	std::uniform_int_distribution<int> _uniform_int_distribution;
};

