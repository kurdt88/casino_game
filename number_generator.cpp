#include "pch.h"
#include "number_generator.h"


NumberGenerator::NumberGenerator(const int &min, const int &max)
	:_default_random_engine(_random_device()), _uniform_int_distribution(min, max)
{
}

NumberGenerator::~NumberGenerator(){}

int NumberGenerator::generate_random_numbers()
{
	return _uniform_int_distribution(_default_random_engine);
}

/*NumberGenerator::NumberGenerator(const NumberGenerator & num)
{
}*/