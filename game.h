#pragma once
#include <iostream>
#include <string>
#include "number_generator.h"

struct Player
{
	std::string name;
	int total_money = 0;
};

struct WagerResult
{
	bool did_win = false;
	int money_change_amount = 0;
	int wager = 0;
	int guess = 0;
	int random_number = 0;
};

class Game
{
	Player Player_;
	NumberGenerator _NumberGenerator;
public:
	Game(const Player& Player_);
	virtual ~Game();
	void star_game();
	bool keep_playing();
	int get_bet_amount();
	int get_guess();
	WagerResult play_around();
	bool game_over();
};