// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "pch.h"
#include <iostream>
#include "game.h"

std::ostream& operator<<(std::ostream& output, const WagerResult &wager)
{
	output << "You bet : " << std::to_string(wager.wager) << ", guessed : "
		<< std::to_string(wager.guess) 
		<< ", random number : " << std::to_string(wager.random_number)
		<< std::endl;
	output << "You ";
	if (wager.did_win)
	{
		output << "won ";
	}
	else
	{
		output << "lost ";
	}
	output << std::to_string(std::abs(wager.money_change_amount)) << std::endl;
	return output;
}

int main()
{	
	std::cout << "Welcome to CASINO" << std::endl;
	
	const Player m_player;
	
	Game *game = new Game(m_player);
	game->star_game();
	do
	{
		const auto result = game->play_around();
		std::cout << result << std::endl;
		if (game->game_over())
		{
			std::cout << "You lost, game over" << std::endl;
			break;
		}

	} while (!game->game_over() && game->keep_playing());
	
	std::cout << "Game over, thank's for palying" << std::endl;
	return 0;
		
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
